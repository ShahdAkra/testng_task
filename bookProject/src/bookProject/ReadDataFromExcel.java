package bookProject;

//reading value of a particular cell  
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadDataFromExcel {

	private static String filePath;

	public ReadDataFromExcel(String filePath) {
		super();
		this.filePath = filePath;
	}

	public static void main(String[] args) throws FileNotFoundException {
	}

	public static int filsRowsNumber() throws FileNotFoundException {
		FileInputStream fis = new FileInputStream(filePath);
		Workbook wb;
		int rowNum = 0;
		try {
			wb = new XSSFWorkbook(fis);
			Sheet sheet = wb.getSheetAt(0); // getting the XSSFSheet object at given index
			rowNum = sheet.getPhysicalNumberOfRows();
		} catch (IOException e) {
			System.out.print("IOException");
			;
		}
		return rowNum;
	}

	// method defined for reading a cell
	public static String readCellData(int vRow, int vColumn) {

		String value = null; // variable for storing the cell value
		Workbook wb = null; // initialize Workbook null
		try {
			// reading data from a file in the form of bytes
			FileInputStream fis = new FileInputStream(filePath);
			// constructs an XSSFWorkbook object, by buffering the whole stream into the
			// memory
			wb = new XSSFWorkbook(fis);
		} catch (FileNotFoundException e) {
			// e.printStackTrace();
			System.out.print("file not found");
		} catch (IOException e1) {
			// e1.printStackTrace();
			System.out.print("IOException");
		}
		try {
			Sheet sheet = wb.getSheetAt(0); // getting the XSSFSheet object at given index
			Row row = sheet.getRow(vRow); // returns the logical row
			Cell cell = row.getCell(vColumn); // getting the cell representing the given column
			value = cell.getStringCellValue(); // getting cell value
		} catch (NullPointerException e) {
			// e.printStackTrace();
			System.out.print("nullPointerException");
		}
		return value; // returns the cell value
	}

	// GET

	public static LinkedList<Integer> getApis() throws FileNotFoundException {

		LinkedList<Integer> list = new LinkedList<Integer>();
		for (int row = 1, col = 0; row <= (filsRowsNumber() - 1); row++) {
			if (readCellData(row, col).equals("get")) {
				list.add(row);
			}
		}
		return list;
	}

	// Delete
	public static LinkedList<Integer> deleteApis() throws FileNotFoundException {

		LinkedList<Integer> list = new LinkedList<Integer>();

		for (int row = 1, col = 0; row <= (filsRowsNumber() - 1); row++) {
			if (readCellData(row, col).equals("delete")) {
				list.add(row);
			}
		}
		return list;
	}

	// PUT
	public static LinkedList<Integer> putApis() throws FileNotFoundException {

		LinkedList<Integer> list = new LinkedList<Integer>();

		for (int row = 1, col = 0; row <= (filsRowsNumber() - 1); row++) {
			if (readCellData(row, col).equals("put")) {
				list.add(row);
			}
		}
		return list;
	}

	// POST
	public static LinkedList<Integer> postApis() throws FileNotFoundException {

		LinkedList<Integer> list = new LinkedList<Integer>();

		for (int row = 1, col = 0; row <= (filsRowsNumber() - 1); row++) {
			if (readCellData(row, col).equals("post")) {
				list.add(row);
			}
		}
		return list;
	}

}
