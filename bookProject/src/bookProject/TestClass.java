package bookProject;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class TestClass {

	private String server;
	ReadDataFromExcel dataFile;

	public void main(String[] args) {
	}

	@BeforeSuite
	public void takingDataFileNameFromUser() throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Please enter Server path: ");// https://fakerestapi.azurewebsites.net
		server = in.readLine();
		System.out.println("Please enter data file path relative to this project:");// TestData.xlsx
		String file = in.readLine();
		dataFile = new ReadDataFromExcel(file);
	}

	@DataProvider(name = "getApisDetails")
	public Object[][] findDataOfGetApis() throws FileNotFoundException {
		LinkedList<Integer> list = dataFile.getApis();
		int size = list.size();
		Object[][] object = new Object[size][2];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < 2; j++) {
				object[i][j] = dataFile.readCellData(list.get(i), (j + 1));
			}
		}
		return object;
	}

	@Test(dataProvider = "getApisDetails")
	public void getTestMethod(String rout, String responseCode) {

		given().when().get(server + "/" + rout).then().assertThat()
				.statusCode(Integer.parseInt(responseCode.substring(1, 4))).and().log().body();
	}

	@DataProvider(name = "deleteApisDetails")
	public Object[][] findDataOfDeleteApis() throws FileNotFoundException {
		LinkedList<Integer> list = dataFile.deleteApis();
		int size = list.size();
		Object[][] object = new Object[size][2];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < 2; j++) {
				object[i][j] = dataFile.readCellData(list.get(i), (j + 1));
			}
		}
		return object;
	}

	@Test(dataProvider = "deleteApisDetails")
	public void deleteTestMethod(String rout, String responseCode) {

		given().when().delete(server + "/" + rout).then().assertThat()
				.statusCode(Integer.parseInt(responseCode.substring(1, 4))).and().log().body();
	}

	@DataProvider(name = "postApisDetails")
	public Object[][] findDataOfPostApis() throws FileNotFoundException {
		LinkedList<Integer> list = dataFile.postApis();
		int size = list.size();
		Object[][] object = new Object[size][8];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < 8; j++) {
				object[i][j] = dataFile.readCellData(list.get(i), (j + 1));
			}
		}
		return object;
	}

	@Test(dataProvider = "postApisDetails")
	public void postTestMethod(String rout, String responseCode, String id, String title, String description,
			String pageCount, String excerpt, String publishDate) {

		given().queryParam("ID", Integer.parseInt(id.replaceAll("\"", ""))).queryParam("Title", title)
				.queryParam("Description", description)
				.queryParam("PageCount", Integer.parseInt(pageCount.replaceAll("\"", "")))
				.queryParam("Excerpt", excerpt).queryParam("PublishDate", publishDate).when().post(server + "/" + rout)
				.then().assertThat().statusCode(Integer.parseInt(responseCode.substring(1, 4))).and().log().body();
	}

	@DataProvider(name = "putApisDetails")
	public Object[][] findDataOfPutApis() throws FileNotFoundException {
		LinkedList<Integer> list = dataFile.putApis();
		int size = list.size();
		Object[][] object = new Object[size][8];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < 8; j++) {
				object[i][j] = dataFile.readCellData(list.get(i), (j + 1));
			}
		}
		return object;
	}

	@Test(dataProvider = "putApisDetails")
	public void putTestMethod(String rout, String responseCode, String id, String title, String description,
			String pageCount, String excerpt, String publishDate) {

		given().queryParam("ID", Integer.parseInt(id.replaceAll("\"", ""))).queryParam("Title", title)
				.queryParam("Description", description)
				.queryParam("PageCount", Integer.parseInt(pageCount.replaceAll("\"", "")))
				.queryParam("Excerpt", excerpt).queryParam("PublishDate", publishDate).when().put(server + "/" + rout)
				.then().assertThat().statusCode(Integer.parseInt(responseCode.substring(1, 4))).and().log().body();
	}

}
